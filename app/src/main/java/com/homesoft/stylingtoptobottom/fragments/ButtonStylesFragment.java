package com.homesoft.stylingtoptobottom.fragments;

import android.app.ActionBar;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.homesoft.stylingtoptobottom.R;

/**
 * Created by dburc on 9/16/2017.
 */

public class ButtonStylesFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_button_styles, container, false);
        final ViewGroup viewGroup = view.findViewById(R.id.layout);
        final Context context = container.getContext();

        //Setting a style programmatically
        final ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(context, R.style.MyButtonStyle);
        final Button button = new AppCompatButton(contextThemeWrapper, null, 0);
        button.setText("Programmatically Styled");
        final ViewGroup.MarginLayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.topMargin = (int)context.getResources().getDimension(R.dimen.top_margin_medium);
        params.setMarginStart((int)context.getResources().getDimension(R.dimen.start_margin_medium));

        viewGroup.addView(button, params);
        return view;
    }
}
