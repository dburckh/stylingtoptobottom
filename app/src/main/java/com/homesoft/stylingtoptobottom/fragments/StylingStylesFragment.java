package com.homesoft.stylingtoptobottom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.homesoft.stylingtoptobottom.R;
import com.homesoft.stylingtoptobottom.ThemedActivity;

/**
 * Created by dburc on 9/15/2017.
 */

public class StylingStylesFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_styling_styles, container, false);

        final View themedActivityButton = view.findViewById(R.id.themedActivityButton);
        themedActivityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().startActivity(new Intent(getActivity(), ThemedActivity.class));
            }
        });

        return view;
    }
}
