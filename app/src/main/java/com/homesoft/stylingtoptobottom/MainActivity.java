package com.homesoft.stylingtoptobottom;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.homesoft.stylingtoptobottom.fragments.ButtonStylesFragment;
import com.homesoft.stylingtoptobottom.fragments.MarginPaddingViewFragment;
import com.homesoft.stylingtoptobottom.fragments.StylingStylesFragment;

public class MainActivity extends AppCompatActivity {
    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager mPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);



    }

    @Override
    protected void onStart() {
        super.onStart();
        final View arrow = findViewById(R.id.arrow);
        arrow.setTranslationX(0f);
        arrow.animate().translationX(getResources().getDimension(R.dimen.arrow_end_margin)).alpha(0f).setDuration(1500);

    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new StylingStylesFragment();
                case 1:
                    return new ButtonStylesFragment();
                case 2:
                    return new MarginPaddingViewFragment();
                default:
                    throw new RuntimeException("No fragment for " + position);

            }
        }

        @Override
        public int getCount() {
            return 3;
        }
    }


}
